<?php
/**
 * 1. 设置session，使用 $_SESSION 超全局变量。
    2. 获取session，使用 $_SESSION 超全局变量。
     3. 删除session，使用unset()或者session_destroy()删除。
 *
 */

session_start();

if (empty($_SESSION['user_id'])){
    echo "请先登录";
    exit();
}

echo "用id：".$_SESSION['user_id'];
echo "</br>";
echo "用户名：".$_SESSION['user_name'];