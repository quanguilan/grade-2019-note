<?php
/**
 * 文章删除页面
 */


//获取分类数据
//$categoryId= $_GET['category_id'];
$categoryId =$_GET["category_id"];


//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");



$sql = "delete from category where category_id = '$categoryId'";
$result = $db->exec($sql);


if($result){
    $log = [
        'category_name' =>null,
        'category_desc' =>null,
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'分类删除成功',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $category_deleteSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$category_deleteSuccess.PHP_EOL,FILE_APPEND);

    echo "删除成功,<a href='category_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "删除失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
    echo "<a href='javascript:history.go(-1);'>返回上一页</a>";
}