<?php
/**
 * 文章增加保存页面
 */


//获取分类数据
$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];


//增加分类名称限制
if(mb_strlen($categoryName) < 3 || mb_strlen($categoryName) > 50 ){
    echo "分类名称限制3-50个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//添加分类描述限制
if (mb_strlen($categoryDesc) <10 || mb_strlen($categoryDesc) >500){
    echo "分类描述限制10-500个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}


//判断名称是否为空
if (empty($categoryName)){
    echo "分类名称为空";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

date_default_timezone_set("PRC");


//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


$addTime  = time();
$updateTime = $addTime;



$sql = "insert into category(category_name,category_desc,update_time,add_time) 
        values('$categoryName','$categoryDesc','$updateTime','$addTime')";
$result = $db->exec($sql);


if($result){
    //记录分类添加成功日志
    $log = [
        'category_name' =>$categoryName,
        'category_desc' =>$categoryDesc,
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'分类添加成功',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $category_addSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$category_addSuccess.PHP_EOL,FILE_APPEND);

    echo "插入成功.<a href='category_list.php'>返回列表页面</a>";
    exit();

}  else{
    $log = [
        'category_name' =>$categoryName,
        'category_desc' =>$categoryDesc,
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'分类添加失败',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $addFail = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$addFail.PHP_EOL,FILE_APPEND);


    echo "插入失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
}