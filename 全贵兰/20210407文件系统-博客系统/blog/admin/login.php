<?php
/**
 *登录页面
 */


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>登录页面</title>
    <link rel="stylesheet" type="text/css" href="css/login.css" />
</head>
<body>
<div id="container">
    <h1>非常日记管理后台</h1>
    <div id="centre">
        <h3>请输入账号和密码</h3>
        <div id="login">
            <ul>
                <form action="login_check.php" method="post">
                    <li>用户<input type="text" name="admin_email" class="textStyle"></li>
                    <li>密码<input type="password" name="admin_password" class="textStyle" ></li>
                    <li>验证码<input type="password" name="admin_code" id="codeStyle">
                        <img src="login_admin_code.php" id="code_img" /></li>
                    <li><input type="checkbox" name="remember_me" class="rememberStyle">记住我</li>
                    <li><input type="submit"  name="btnSubmit" value="登录" class="btnStyle"</li>
                </form>
            </ul>
        </div>
    </div>
    <div id="create">
        <a href="#">新员工？创建账号</a>
    </div>

</div>



</body>
</html>

