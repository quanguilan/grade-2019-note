<?php
/**
 * 文章编辑保存页面
 */



//获取分类数据
$articleId= $_POST['article_id'];
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$articleIntro = $_POST['article_intro'];
$articleContent = $_POST['article_content'];




//文章内容所属限制
if(empty($articleId)){
    echo "文章编号不能为空";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

if(mb_strlen($articleTitle) < 5 || mb_strlen($articleTitle) > 50 ){
    echo "文章标题限制5-50个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
if (mb_strlen($articleIntro) <10 || mb_strlen($articleIntro) >500){
    echo "文章简介限制10-500个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

if (mb_strlen($articleContent) < 10 || mb_strlen($articleContent) >8000){
    echo "文章内容限制10-8000";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}


//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


$updateTime = time();

$sql = "update article set article_id ='$articleId' ,article_title = '$articleTitle' ,
                   category_id='$categoryId', article_intro= '$articleIntro',
                   article_content='$articleContent',update_time ='$updateTime' 
                   where article_id = '$articleId'";
$result = $db->exec($sql);
//var_dump($result);

if($result){
//文章编辑成功日志
    $log = [
        'article_title' =>$articleTitle,
        'category_id' =>$category_id,
        'article_intro' =>$articleIntro,
        'article_content' =>$articleContent,
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'文章编辑成功',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $article_addSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$article_addSuccess.PHP_EOL,FILE_APPEND);

    echo "编辑文章成功,<a href='article_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "编辑文章失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
}