<?php
/**
 * 管理员编辑保存页面
 */


//获取分类数据
$admin_id = $_POST['admin_id'];
$admin_name = $_POST['admin_name'];
$admin_email = $_POST['admin_email'];
$admin_password = $_POST['admin_password'];
$admin_rePassword = $_POST['admin_rePassword'];


//判断输入密码是否一致
if ($admin_password !== $admin_rePassword ){
    echo "请输入一致的密码";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//判断管理员名称
if(mb_strlen($admin_name) < 3 || mb_strlen($admin_name) > 20 ){
    echo "管理员名称限制3-20个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//判断管理员邮箱
//if (mb_strlen($admin_email) <10 || mb_strlen($admin_email) >30){
//    echo "管理员邮箱10-30个字";
//    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
//    exit();
//}

//判断管理员密码
if (mb_strlen($admin_password) < 6 || mb_strlen($admin_password) >20){
    echo "管理员密码限制6-20";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}


//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


$update_time = time();

//更新管理员编辑
$sql = "update admin set admin_name ='$admin_name' ,admin_email ='$admin_email',admin_password ='$admin_password',update_time = '$update_time'
       where admin_id = '$admin_id'";
$result = $db->exec($sql);
//var_dump($result);

if($result){
    echo "修改成功,<a href='Administrator_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "修改失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
}