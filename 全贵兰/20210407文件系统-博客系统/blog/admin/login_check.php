<?php
/**
 * 登录检查页面
 */

/**
 * 检查登录
 */
$adminEmail = $_POST['admin_email'];
$adminPassword =$_POST['admin_password'];
$adminCode = $_POST['admin_code'];
$rememberMe = $_POST['remember_me'] ?? '';
//var_dump($adminCode);
//var_dump($rememberMe);



//开启session
session_start();
if ($adminCode != $_SESSION['verify_code']) {
    echo "验证码错误。<a href='login.php'>返回登录页</a>";
    exit();
}


// 设置时区
date_default_timezone_set("PRC");


//判断是否为空
if (empty($adminEmail)){
    echo "账号和密码不匹配 <a href='login.php'>返回登录页面</a>";
    exit();
}


//连接MySQL数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


//从管理员表查找是否有这个账户名
$sql = "select * from admin  where admin_email='{$adminEmail}'";
$result = $db ->query($sql);
$adminInfo = $result ->fetch(PDO::FETCH_ASSOC);


//判断密码是否正确
if ($adminInfo['admin_password'] == $adminPassword ){
    if (!session_id()) {
        session_start();
    }
    $_SESSION['admin_email'] = $adminEmail;
    $_SESSION['admin_name'] = $adminInfo['admin_name'];

    if ($rememberMe){
        setcookie("PHPSESSID",session_id(),time()+5);
    }

    //记录登录成功日志
    $log = [
        'admin_email' =>$_SESSION['admin_email'],
        'admin_name' =>$_SESSION['admin_name'],
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'登录后台成功',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $loginSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$loginSuccess.PHP_EOL,FILE_APPEND);

    echo "登录成功";
    echo "<a href='category_list.php'>前往分类列表页面<a/>";
    exit();
   } else{

    //记录登录失败日志
    $log = [
        'admin_email' =>$_SESSION['admin_email'],
        'admin_name' =>$_SESSION['admin_name'],
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'登录后台失败',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $loginFail = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$loginFail.PHP_EOL,FILE_APPEND);


    echo "登录失败，账号或者密码错误<br/>";
    echo "<a href='login.php'>请重新登录</a>";
    exit();
}