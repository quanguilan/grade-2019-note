<?php
/**
 *文章列表页面
 */

session_start();
//连接MySQL数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


//规定一个由 date_create() 返回的 DateTime 对象。该函数修改该对象。
date_default_timezone_set("PRC");

//查询article文章列表
$sql= "select * from article order by article_id desc";
$result = $db->query($sql);
$articleList = $result->fetchALL(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客系统</h1>
        <div id="admin-info">欢迎您，<?php echo $_SESSION['admin_name']?>
            <a href="logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="Administrator_list.php">管理员管理</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="shoye">
            <a href="article_list.php">首页</a> &gt;
            <a href="article_list.php">文章管理</a> &gt;
            <a href="article_add.php">增加文章</a> &gt;
        </div>
        <div id="table-menu">
            <button class="btn">全选</button>
            <a href="#">删除选择任务</a>
            <div id="increase"><a href="article_add.php">增加文章</a></div>
        </div>
        <div id="table-list">
            <table width="99%" border="1" cellspacing="0" cellpadding="0" >
                <tr>
                    <th></th>
                    <th>文章id</th>
                    <th>文章标题</th>
                    <th>所属分类</th>
                    <th>文章简介</th>
                    <th>发表时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($articleList as $row):?>
                <tr>
                    <td><input type="checkbox"></td>
                    <td><?php echo $row['article_id'];?></td>
                    <td><?php echo $row['article_title'];?></td>

                    <td><?php
                        $sql = "select * from category where category_id = '{$row['category_id']}'";
                       $result = $db->query($sql);
                       $category = $result ->fetch(PDO::FETCH_ASSOC);
                       echo $category['category_name'];
                    ?>
                    </td>
                    <td><?php echo $row['article_intro'];?></td>
                    <td><?php echo date("Y-m-s H:i:s ",$row['add_time'] );?></td>
                    <td><?php echo date("Y-m-s H:i:s",$row['update_time']);?></td>
                    <td>
                        <a href="article_edit.php?article_id=<?php echo $row['article_id'];?>">编辑</a>
                        <a href="article_delete.php?article_id=<?php echo $row['article_id'];?>">删除</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
</body>
</html>

