<?php
/**
 *
 *文章列表
 */
session_start();
//判断一下有没有登录，阻止匿名用户直接访问后台页面
if (empty($_SESSION['admin_email'])){
    echo "你当前没有登录，请先登录<br/>";
    echo "<a href='login.php'>返回登录页面</a>";
    exit();
}

//连接MySQL数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

//设置时区  规定一个由 date_create() 返回的 DateTime 对象。该函数修改该对象。
date_default_timezone_set("PRC");


//查询MySQL数据
$sql= "select * from category order by category_id desc";
$result = $db->query($sql);
$categoryList=$result->fetchALL(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客系统</h1>
        <div id="admin-info">欢迎您，<?php echo $_SESSION['admin_name']?>
            <a href="logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="Administrator_list.php">管理员管理</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="shoye">
            <a href="#">首页</a> &gt;
            <a href="category_list.php">分类管理</a> &gt;
            <a href="category_add.php">分类增加</a>
        </div>
        <div id="table-menu">
            <button class="btn">全选</button>
            <a href="#">删除选择任务</a>
            <div id="increase"><a href="category_add.php">增加分类</a></div>
        </div>
        <div id="table-list">
            <table width="99%" border="1" cellspacing="0" cellpadding="0" >
                <tr>
                    <th></th>
                    <th>分类id</th>
                    <th>分类名称</th>
                    <td>分类描述</td>
                    <th>发表时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($categoryList as $row):?>

                <tr>
                    <td><input type="checkbox"></td>
                    <td><?php echo $row['category_id'];?></td>
                    <td><?php echo $row['category_name'];?></td>
                    <td><?php echo $row['category_desc'];?></td>
                    <td><?php echo $row['update_time'];?></td>
                    <td><?php echo $row['add_time'];?></td>

                    <td><a href="category_edit.php?category_id=<?php echo $row['category_id']; ?>">编辑</a>
                        <a href="category_delete.php?category_id=<?php echo $row['category_id'];?>">删除</a>
                    </td>

                </tr>
                <?php endforeach;?>

            </table>
        </div>
    </div>
</div>
</body>
</html>

