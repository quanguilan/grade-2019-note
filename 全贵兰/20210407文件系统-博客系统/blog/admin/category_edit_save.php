<?php
/**
 * 文章编辑保存页面，
 */


//获取分类数据
$categoryId= $_POST['category_id'];
$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];


//增加分类内容限制
if(mb_strlen($categoryName) < 5 || mb_strlen($categoryName) > 50 ){
    echo "分类名称限制5-50个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
if (mb_strlen($categoryDesc) <10 || mb_strlen($categoryDesc) >500){
    echo "分类描述限制10-500个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}


//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


$updateTime = time();

$sql = "update category set category_name ='$categoryName' ,category_desc = '$categoryDesc' ,update_time ='$updateTime' 
       where category_id = '$categoryId'";
$result = $db->exec($sql);
//var_dump($result);

if($result){
    //分类编辑添加日志
    $log = [
        'category_name' =>$categoryName,
        'category_desc' =>$categoryDesc,
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'分类编辑成功',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $category_editSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$category_editSuccess.PHP_EOL,FILE_APPEND);

    echo "修改成功,<a href='category_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "修改失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
}