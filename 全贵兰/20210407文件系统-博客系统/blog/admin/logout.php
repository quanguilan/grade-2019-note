<?php
/**
 * 退出登录页面
 */

//setcookie("AdminId","");
//setcookie("AdminAccount","");

session_start();
//删除账号和名称
unset($_SESSION['admin_email']);
unset($_SESSION['admin_name']);

//记录退出成功日志
if(empty($_SESSION['admin_email]'])){
    $log = [
        'admin_email' =>null,
        'admin_name' =>null,
        'ip' =>$_SERVER['REMOTE_ADDR'],
        'action' =>'login',
        'content' =>'退出登录成功',
        'time' =>date("Y-m-d H:i:s",time())
    ];
    $logoutSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents("log.txt",$logoutSuccess.PHP_EOL,FILE_APPEND);
}


echo "退出登录成功<br/>";
echo "<a href='login.php'>返回登录页面</a>";