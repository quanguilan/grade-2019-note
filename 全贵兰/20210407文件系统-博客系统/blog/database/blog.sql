-- 创建数据库blog
CREATE DATABASE IF NOT EXISTS blog
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

-- 使用blog
use blog;

-- 创建分类表
CREATE TABLE `category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(300) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='分类表';


-- ----------------------------
INSERT INTO `category` VALUES ('3', '凡是过往，皆为序章', '黄色的冬日来了，充满厌烦', '1616545909', '1616545909');
INSERT INTO `category` VALUES ('4', '宜遐想', '加缪说过，诞生到一个荒谬世界上来的人 唯一真正的职责是活下去，是意识到自己的生命。', '1616546028', '1616546028');
INSERT INTO `category` VALUES ('5', 'quanguilan', '打发第三方第三方对方答复', '1616552254', '1616552254');


-- 创建文章表
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(30) NOT NULL COMMENT '文章标题',
  `article_intro` varchar(255) DEFAULT NULL COMMENT '文章简介',
  `article_content` varchar(1000) NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='文章表';



INSERT INTO `article` VALUES ('7', '3', '目光的第二次给予', '热太热太热太热', '热太热太热有人', '1616552286', '1616552286');
INSERT INTO `article` VALUES ('8', '3', '宜豪情', '让他热一热', 'ytru6ityi', '1616552296', '1616552296');
INSERT INTO `article` VALUES ('9', '3', '鞘安愚钝，以胡剑利y', '语言兔兔', '体育与一体', '1616552306', '1616552306');
INSERT INTO `article` VALUES ('10', '3', '法规和规范孤鸿寡鹄', '非官方大哥发的', '都发给我让他让他', '1616552352', '1616552352');


-- 创建用户信息表
CREATE TABLE `admin` (
  `admin_id` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `admin_email` varchar(50)not null COMMENT '用户邮箱',
  `admin_name` varchar(50) NOT NULL COMMENT  '用户名称',
  `admin_password` varchar(50) NOT NULL COMMENT '用户密码',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- drop table admin;

INSERT INTO `admin` VALUES ('1','2218922949@qq.com', 'admin', '123456','1616552306','1616552306' );
INSERT INTO `admin` VALUES ('2', '2218922949@qq.com','全贵兰', '123456', '1616552306','1616552306');

