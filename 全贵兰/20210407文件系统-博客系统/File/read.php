<?php
//读取文件
//示例一   从上例的log。txt逐行读取内容

//feof()函数检测是否以达到文件末尾 如果达到返回true，如果没达到 返回false
//fgets（）用于从文件中逐行读取文件

$file = fopen("log.txt","r");//以追加的方式打开一个文焕
if ($file){  //如果文件打开成功
    while(!feof($file)){ //读取文件每一行，直到文件结束
        echo fgets($file) ."</br>";
    }
  fclose($file);
}  else{
    echo "打开文件失败";
}





