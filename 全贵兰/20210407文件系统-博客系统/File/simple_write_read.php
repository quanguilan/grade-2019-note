<?php
//如果只是简单的文件写入和读取操作，
//那么会使用file_get_contents和fit_put_contents函数。
//file_put_contents函数把一个字符串写入文件中，参数：
//1. 第1个表示文件名。
//2. 第2个表示写入的内容。
//3. 第3个表示写入的方式，其中 FILE_APPEND 就是表示追加在文件后面的意思。

//示例，往log.txt文件写入内容 hello php file_put_contents：
//file_put_contents("log.txt","hello php file_put_contents".PHP_EOL,FILE_APPEND);



//file_get_contents
//示例，读取log.txt内容，将换行符号 \n 替换成 <br>，并在页面显示：

 $content = file_get_contents("log.txt");
 echo str_replace(PHP_EOL,"</br>",$content);





