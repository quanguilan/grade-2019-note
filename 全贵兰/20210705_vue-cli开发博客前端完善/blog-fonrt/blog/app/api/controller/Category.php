<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;

class Category
{
    public function list()
    {
        $categoryList = CategoryModel::select();
        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'categoryList' => $categoryList,
            ],
        ];
        return json($data);
    }
}
