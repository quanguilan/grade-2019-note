import Vue from 'vue'
import App from './App.vue'
import "./assets/bootstrap/css/bootstrap.min.css";
import "./assets/css/cl-css.css";





// 1. 引入路由类库
import VueRouter from 'vue-router';
Vue.use(VueRouter); // Vue框架默认不开启路由，需要使用这个路由

// 2. 配置路由规则
import Index from './components/Index.vue'; // 引入首页组件
import Detail from './components/Detail.vue'; // 引入详情页组件
import List from './components/List.vue'; // 引入列表组件
const routes = [{  // 路由规则
			path: '', // 访问路径   设置默认为index
			component: Index // 渲染组件
		},
		{  // 路由规则
		path: '/index', // 访问路径
		component: Index // 渲染组件
	},
	{
		path: "/detail",
		component: Detail
	},
	{
		path: "/list",
		component: List
	}
];

// 3. 实例化路由对象
const router = new VueRouter({ // 实例化这个路由对象
	routes // 这里变量名必须叫做 routes
});


Vue.config.productionTip = false

new Vue({
	router, // 引入这个route
  render: h => h(App),
}).$mount('#app')
