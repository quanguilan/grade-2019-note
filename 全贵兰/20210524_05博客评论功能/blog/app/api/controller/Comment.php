<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    //增加评论
    public function add()
    {
        $params = Request::param();
        $validate = Validate::rule([
           'comment_content|评论内容' =>'require|min:1|max:500',
           'email|邮箱' =>'require|min:3|max:100',
           'nickname|昵称' =>'require|min:1|max:20',
            'article_id|文章id|'=>'require|between:1,'.PHP_INT_MAX,
        ]);
        if (!$validate ->check($params)){
            $data = [
              'status' =>1002,
              'message' =>$validate ->getError(),
              'data' =>[],
            ];
            return json($data);
        }


        //验证评论内容是否合规
        require_once '../extend/baiduai/AipContentCensor.php';
       // 你的 APPID AK SK
        $client = new \AipContentCensor(24190641, "AQFrOHEM9OL2xmCcGMFqhf3D", "9WCTPxhGZGuSr45iG4t7XHC48ATC5EQi");
        $result = $client->textCensorUserDefined($params['comment_content'].''.$params['email'].''.$params['nickname']);

        if($result['conclusionType'] == 2){  //不合规
           $data = [
             'status' =>10003,
             'message' =>'评论内容包含敏感词',
             'data' =>[],
           ];
           return json($data);
       }
        if ($result['conclusionType'] == 3){ //疑似
            $insertData = $params;
            $insertData['add_time'] = $insertData['update_time'] = time();
            $insertData['status'] =2;
            CommentModel::create($insertData);
            $data =[
                'status' =>10003,
                'message' =>'评论内容包含敏感词',
                'data' =>[],
            ];
           return json($data);
        }


        //写入数据库
//        $params['add_time'] = time();
//        $params['update_time'] = time();

        $insertData = $params;
        $insertData['add_time'] = $insertData['update_time'] = time();
        $result = CommentModel::create($insertData);

        //返回json数据格式
        if($result){
            $data = [
                'status' =>0,
                'message' =>'success',
                'data' =>[
                    'result' =>true,
                    'comment' =>$result->toArray(),
                ]
            ];
            return json($data);
        }
        $data = [
            'status' =>1001,
            'message' =>'系统错误，数据库处理失败',
            'data' =>[],
        ];
        return json($data);

    }
    //评论列表
    public function list(){
        //接收article_id
        $articleId = Request::param('article_id');
        $validate = Validate::rule([
           'article_id' =>'require|between:1,'.PHP_INT_MAX,
        ]);
        if (!$validate ->check(['article_id' =>$articleId])){
            $data = [
                'status' =>1002,
                'message' =>$validate ->getError(),
                'data' =>[],
            ];
            return json($data);
        }
        //从数据库中读取出来
       $result= CommentModel::where("article_id","=",$articleId)->order('add_time','desc')->select();
        //遍历数组
        $commentList = [];
        foreach ($result as $item){
            $temp = $item->toArray();

            $commentList[] = [
              'comment_id' =>$temp['comment_id'],
                'comment_content' =>$temp['comment_content'],
                'nickname' =>$temp['nickname'],
                'add_time' =>$temp['add_time'],
            ];
        }
        $data =[
            'status' =>0,
            'message' =>'success',
            'data' =>[
                'commentList' =>$commentList,
            ]
        ];
        return json($data);
    }
}