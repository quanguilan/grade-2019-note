    server {
        listen       8901;
        server_name  localhost;
        location / {
            root   html/blog/public;
            index  index.html index.htm index.php;
						if (!-e $request_filename) {
							rewrite  ^(.*)$  /index.php?s=/$1  last;
						}
        }
        location ~ \.php$ {
            root           html/blog/public;
            fastcgi_pass   127.0.0.1:9001;
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
            include        fastcgi_params;
        }
    }