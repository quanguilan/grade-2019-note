<?php
/**
 * 增加管理员页面
 */



//连接MySQL数据库
//$dsn = "mysql:host=127.0.0.1;dbname=blog";
//$db = new PDO($dsn, "root", "qgl3661766436");
//$db->exec("set names utf8mb4");

//设置时区  规定一个由 date_create() 返回的 DateTime 对象。该函数修改该对象。
date_default_timezone_set("PRC");

//查询MySQL数据
//$sql= "select * from admin order by admin_id desc";
//$result = $db->query($sql);
//$adminList=$result->fetchALL(PDO::FETCH_ASSOC);


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客系统</h1>
        <div id="admin-info">欢迎您,quanguilan
            <a href="logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a>管理员管理</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="shoye">
            <a href="Administrator_list.php">首页</a> &gt;
            <a href="Administrator_list.php">管理员管理</a> &gt;
            <a href="Administrator_edit.php">增加管理员</a>
        </div>
        <div id="table_add">
            <form action="Administrator_add_save.php" method="post">
                <table border="=1" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>姓名：</td>
                        <td><input type="text" name="admin_name" </td>
                    </tr>
                    <tr>
                        <td>邮箱：</td>
                        <td><input type="text" name="admin_email" </td>
                    </tr>
                    <tr>
                        <td>密码：</td>
                        <td><input type="text" name="admin_password" </td>
                    </tr>
                    <tr>
                        <td>确认密码：</td>
                        <td><input type="text" name="admin_rePassword" </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" value="保存" class="btn" />
                            <input type= "reset" value="重置" class="btn" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        </div>
    </div>
</div>
</body>
</html>


