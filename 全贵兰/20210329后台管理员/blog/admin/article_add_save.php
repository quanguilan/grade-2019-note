<?php
/**
 * 文章增加保存页面
 */
//var_dump($_POST);
//exit();
//获取分类数据
$articleTitle = $_POST['article_title'];
$category_id = $_POST['category_id'];
$articleIntro = $_POST['article_intro'];
$articleContent = $_POST['article_content'];


if(mb_strlen($articleTitle) < 5 || mb_strlen($articleTitle) > 50 ){
    echo "文章标题限制5-50个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
if (mb_strlen($articleIntro) <10 || mb_strlen($articleIntro) >500){
    echo "文章简介限制10-500个字";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

if (mb_strlen($articleContent) < 10 || mb_strlen($articleContent) >8000){
    echo "文章内容限制10-8000";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}


if (empty($category_id)){
    echo "需要选择文章分类";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}


//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "qgl3661766436");
$db->exec("set names utf8mb4");
//var_dump($db);

$addTime = time();
$updateTime = $addTime;

$sql = "insert into article(article_title,category_id,article_intro,article_content,add_time,update_Time) 
        values('$articleTitle','$category_id','$articleIntro','$articleContent','$addTime','$updateTime')";
$result = $db->exec($sql);
//var_dump($result);

if($result){
    echo "插入成功.<a href='article_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "插入失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
}