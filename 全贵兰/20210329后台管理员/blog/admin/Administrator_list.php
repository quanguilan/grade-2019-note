<?php
/**
 *管理员账户页面
 */


//连接MySQL数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "qgl3661766436");
$db->exec("set names utf8mb4");

//设置时区  规定一个由 date_create() 返回的 DateTime 对象。该函数修改该对象。
date_default_timezone_set("PRC");


//查询MySQL数据
$sql= "select * from admin order by admin_id desc";
$result = $db->query($sql);
$adminList=$result->fetchALL(PDO::FETCH_ASSOC);


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客系统</h1>
        <div id="admin-info">欢迎您,quanguilan
            <a href="logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a>管理员管理</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="shoye">
        </div>
        <div id="table-menu">
            <div id="increase"><a href="Administrator_add.php">增加管理员</a></div>
        </div>
        <div id="table-list">
            <table width="99%" border="1" cellspacing="0" cellpadding="0" >
                <tr>
                    <th></th>
                    <th>管理员id</th>
                    <th>管理员名称</th>
                    <th>管理员邮箱</th>
                    <th>增加时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($adminList as $row):?>

                    <tr>
                        <td><input type="checkbox"></td>
                        <td><?php echo $row['admin_id'];?></td>
                        <td><?php echo $row['admin_name'];?></td>
                        <td><?php echo $row['admin_email'];?></td>
                        <td><?php echo $row['update_time'];?></td>
                        <td><?php echo $row['add_time'];?></td>

                        <td><a href="Administrator_edit.php?admin_id=<?php echo $row['admin_id']; ?>">编辑</a>
                        </td>

                    </tr>
                <?php endforeach;?>

            </table>
        </div>
    </div>
</div>
</body>
</html>


