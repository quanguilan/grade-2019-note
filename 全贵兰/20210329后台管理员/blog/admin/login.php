<?php
/**
 *登录页面
 */


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="css/base.css" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="outside">
        <h3>员工OA系统</h3>
        <div id="inside">
            <h5>请输入账号和密码</h5>
            <ul>
                <form action="login_check.php" method="post">
                    <li>账号<input name="admin_email" type="text" class="textStyle" </li>
                    <li>密码<input name="admin_password" type="text" class="textStyle"</li>
                    <li><input name="remember_me" type="checkbox" class="textRemember">记住我</li>
                    <li><input name="btnSubmit" type="submit" value="登录" class="btnStyle"</li>
                </form>
            </ul>
        </div>
        <div id="create">
            <a href="register.php">新员工？创建账号</a>
        </div>
    </div>
</div>
</body>
</html>


