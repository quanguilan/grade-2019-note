<?php
/**
 * 文章编辑页面
 */

//接收到数据
$categoryId =$_GET["category_id"];

//连接数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "qgl3661766436");
$db->exec("set names utf8mb4");

$sql = "select * from category where category_id = '$categoryId'";
$result = $db->query($sql);
$category = $result->fetch(PDO::FETCH_ASSOC);
//var_dump($category);
//exit();

?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="css/main.css" type="text/css"/>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<h1>博客系统</h1>
				<div id="admin-info">欢迎您，quanguilan<a href="logout.php">退出登录</a></div>
			</div>
			<div id="left">
				<ul>
					<li><a href="category_list.php">分类管理</a></li>
					<li><a>新闻管理</a></li>
					<li><a href="Administrator_list.php">管理员管理</a></li>
				</ul>
			</div>
			<div id="right">
				<div id="shoye">
					<a href="#">首页</a> &gt;
					<a href="#">分类管理</a> &gt;
					<a href="#">编辑分类</a>
				</div>
				<div id="table_add">
                    <form action="category_edit_save.php" method="post">
						<table border="=1" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="190">分类id:</td>
                                <td><input type="text" name="category_id" value="<?php echo $category['category_id']; ?>" readonly="readonly" /> </td>
                            </tr>

							<tr>
								<td>分类名称：</td>
								<td><input type="text" name="category_name" value="<?php echo $category['category_name']; ?>" /></td>
							</tr>

							<tr>
								<td>分类描述：</td>
                                <td>
                                    <textarea name="category_desc" value="<?php echo $category['category_desc']; ?>" /></textarea>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="submit" value="提交" class="btn" />
									<input type= "reset" value="重置" class="btn" />
								</td>
							</tr>
						</table>
                    </form>
	                </div>
					</div>
			</div>
		</div>
		</body>
</html>
