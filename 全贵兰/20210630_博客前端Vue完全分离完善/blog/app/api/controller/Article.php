<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\ArticleModel;
use think\facade\Request;
use think\facade\Validate;

class Article
{
    // 文章列表
    public function list()
    {
        $params = Request::param();
        $params['category_id'] = $params['category_id'] ?? 0;
        $params['page'] = $params['page'] ?? 1;
        $params['page_size'] = $params['page_size'] ?? 2;
        $validate = Validate::rule([
            'page' => 'between:1,' . PHP_INT_MAX,
            'page_size' => 'between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }

//        $articleList = ArticleModel::where('category_id', '=', 1)
//            ->order('add_time desc')
//            ->paginate(['list_rows' => 10]);
//        var_dump($articleList);exit();

        $model = ArticleModel::where('article_id', '>', 0);
        if ($params['category_id']){
            $model->where('category_id','=',$params['category_id']);
        }
            $result = $model->order('add_time desc')
            ->paginate(['list_rows' => $params['page_size'], 'page' => $params['page']]);

        $articleList = [];
        foreach ($result as $item) {
            $articleList[] = $item;
        }

        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'articleList' => $articleList,
                'total' => $result->total()
            ],
        ];
        return json($data);
    }

    // 文章详情接口
    public function detail()
    {
        $articleId = Request::param("article_id");
        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['article_id' => $articleId])) {
            $data = [
                'status' => 10001,
                'message' => $validate->getError(),
                'data' => [],
            ];
            return json($data);
        }

        $article = ArticleModel::find($articleId);
        if (!$article) {
            $data = [
                'status' => 11001,
                'message' => '文章不存在',
                'data' => [],
            ];
            return json($data);
        }

        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'article' => $article,
            ],
        ];
        return json($data);
    }


}
