<?php
//1. 设计表单页面，用户可以输入字符串，然后使用ajax调用服务端接口计算长度，并把结果显示在页面上。
if (empty($_POST['username'])){
    $data  =[
        'status' =>1,
        'message' =>'用户名为空',
        'data' =>[],
    ];
    header("Content-type:application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else{
    $str = mb_strlen($_POST['username']);
    $data = [
        'status' =>1,
        'message' =>'success',
        'data' =>[
            'username' =>$_POST['username'],
            'str'  => $str,
        ]
    ];
    header("Content-Type:application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}
