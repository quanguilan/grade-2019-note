<?php
//2. 设计表单页面，用户可以输入英文文章，然后使用ajax调用服务端接口统计词频，并把结果显示在页面上。

$word = array_values(array_unique(explode(" ", strtolower($_POST['EnglishArticle']))));
$data = [
    'status' => 1,
    'message' => 'success',
    'data' => [
        'EnglishArticle' =>$_POST['EnglishArticle'],
        'word' =>$word,
    ]
];
header("Content-Type:application/json");
echo json_encode($data,JSON_UNESCAPED_UNICODE);
