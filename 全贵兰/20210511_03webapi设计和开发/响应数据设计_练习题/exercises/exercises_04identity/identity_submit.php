<?php
//4. 设计表单页面，用户可以输入身份证，然后使用ajax调用服务端身份证分析接口，计算年龄和性别，并把结果显示在页面上。
//判断是否为空
if (empty($_POST['identityNum'])){
    $data = [
        'status' =>1,
        'message' =>'身份证不能为空',
        'data' =>[],
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);

//判断是否等于18
}else if(mb_strlen($_POST['identityNum']) !==18){
    $data = [
        'status' => 1,
        'message' => '身份证号不正确',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else{
    $arr = array_map("intval",str_split($_POST['identityNum']));
    $sex = $arr[17-1];
    $birthday = '';
    for($i = 6;$i<=9;$i++){
        $birthday .=$arr[$i];
    };
    //获取当前年份
    $nowYear = date("Y");
    //获取生日年份
    $birthYear = $birthday;
    $age = $nowYear - $birthYear;

    if ($sex % 2 == 0){
        $sunSex = "女";
    }else{
        $sunSex = "男";
    }
    $data = [
      'status' =>1,
      'message' =>'success',
      'data' =>[
          'age' =>$age,
          'sex' =>$sunSex,
      ]
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);

}

