<?php
declare (strict_types = 1);

namespace app\api\controller;

use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Index
{
    public function index()
    {
        return '您好！这是一个[api]示例应用';
    }

    public function sample()
    {
        $data = [
            'status' => 0,
            'message' => '我只是一个美丽的错误',
            'data' => [],
        ];
        return json($data);
    }


//1. 接第一章节练习题第2题，现在需要改进接口，移除首尾的空格再计算长度，请使用版本设计的方式改进。
//例如：传入 web api，返回结果
    public function strLength()
    {
        $v = Request::param("v", 1);
        if ($v == 1) { //当版本等于2时
            //获取参数
            $str = Request::param("str");
            //参数校验
            $validate = Validate::rule([
                'str|字符串' => 'require',
            ]);
            if (!$validate->check(['str' => $str])) {
                $data = [
                    'status' => 1,
                    'message' => '字符串不能为空',
                    'data' => [],
                ];
                return json($data);

            }
            //计算传入字符串长度
            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
                    'length' => mb_strlen($str),
                ]
            ];
            return json($data);

        } else if ($v == 2) { //当版本等于2时
            //获取参数
            $str = Request::param("str");
            //参数校验
            $validate = Validate::rule([
                'str|字符串' => 'require',
            ]);
            if (!$validate->check(['str' => $str])) {
                $data = [
                    'status' => 1,
                    'message' => '字符串不能为空',
                    'data' => [],
                ];
                return json($data);

            }
            //计算传入字符串长度
            $strLen = trim($str, " "); //移除前后空格
            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
                    'length' => mb_strlen($strLen),
                ]
            ];
            return json($data);
        }
    }


//设计一个api接口，可以分析一篇英文文章出现的单词列表，并返回json格式。
//第一种写法：
//    public function word()
//    {
//        $v = Request::param("v", 1);
//        if ($v == 1) {
//            return $this->wordv1();
//        } else if ($v == 2) {
//            return $this->wordv2();
//        } else if ($v == 3) {
//            return $this->wordv3();
//        }
//    }
//
//    public function wordv1()
//    {
//        $content = Request::param("content");
//        $validate = Validate::rule([
//            'content|文章内容' => 'require|min:10|max:100',
//        ]);
//        if (!$validate->check(['content' => $content])) {
//            $date = [
//                'status' => 1,
//                'message' => $validate->getError(),
//                'data' => [],
//            ];
//            return json($date);
//        }
//        $words = array_values(array_unique(
//            explode(" ", strtolower($content))));
//        $data = [
//            'status' => 1,
//            'message' => $validate->getError(),
//            'data' => $words,
//        ];
//        return json($data);
//    }
//
//    public function wordv2()
//    {
//        $content = Request::param("content");
//        $validate = Validate::rule([
//            'content|文章内容' => 'require|min:10|max:100',
//        ]);
//        if (!$validate->check(['content' => $content])) {
//            $data = [
//                'status' => 1,
//                'message' => $validate->getError(),
//                'data' => [],
//            ];
//            return json($data);
//        }
//        $words = explode(" ", strtolower($content));
//        $result = [];   //存放每个单词出现的次数
//        foreach ($words as $word) {
//            if (empty($result[$word])) {
//                $result[$word] = 0;  //默认所以单词次数为零
//            }
//            $result[$word]++;
//        }
//        $data = [
//            'status' => 1,
//            'message' => $validate->getError(),
//            'data' => $result,
//        ];
//        return json($data);
//    }
//}
//2. 接第一章节练习题第4题，现在需要改进接口，返回如下数据：
//第二种写法：
    public function word()
    {
        $v = Request::param("v", 1);
        if ($v == 1) { //版本等于1时
            $content = Request::param("content");
            $validate = Validate::rule([
                'content|文章内容' => 'require|min:10|max:100',
            ]);
            if (!$validate->check(['content' => $content])) {
                $date = [
                    'status' => 1,
                    'message' => $validate->getError(),
                    'data' => [],
                ];
                return json($date);
            }
            $words = array_values(array_unique(
                explode(" ", strtolower($content))));
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => $words,
            ];
            return json($data);
        }else if($v == 2){
            $content = Request::param("content");
            $validate = Validate::rule([
                'content|文章内容' =>'require|min:10|max:100',
            ]);
            if (!$validate ->check(['content' =>$content])){
                $data = [
                    'status' =>1,
                    'message' =>$validate ->getError(),
                    'data' =>[],
                ];
                return json($data);
            }
            $words = explode(" ",strtolower($content));
            $result = [];   //存放每个单词出现的次数
            foreach ($words as $word){
                if (empty($result[$word])){
                    $result[$word] = 0;  //默认所以单词次数为零
                }
                $result[$word]++;
            }
            $data = [
                'status' => 1,
                'message' =>$validate ->getError(),
                'data' =>$result,
            ];
            return json($data);
        }
    }
}



