<?php
/**
 * 登录成功页面
 */

date_default_timezone_set("PRC");

if (empty($_COOKIE['user_name'])){
    echo "请输入用户名";
    exit();
}
if (empty($_COOKIE['user_password'])){
    echo "请输入密码";
    exit();
}

$userName = $_POST['user_name'];
$userPassword = $_POST['user_password'];

if ($_COOKIE['user_name'] == $userName && $_COOKIE['user_password'] == $userPassword){
    echo "登录成功";
    echo "</br>";
    echo "用户名：". $_COOKIE['user_name'];
    echo "</br>";
    echo "密码：".$_COOKIE['user_password'];
    echo "</br>";
    echo "登录时间是：".$_COOKIE['lastvisit'];
}else{
    echo "登录失败<a href='login.php'>返回登录页面</a>";
    exit();
}



//echo $_COOKIE[time()];
//echo setCookie("lastvisit",date("Y-m-d H:i:s"),time()+3600*24*360);