<?php
/**
 *
 * php中没有直接删除cookie得函数，但是通过设置过期时间可以变相实现。

示例1，删除名称为 "userName" 的 cookie，使过期日期变更为过去的时间点：
 *
 */

//1、删除cookie

setcookie("user_name022","",time()-3600);


//主要内容：
//  1. 设置cookie，使用setcookie()函数。
//	2. 获取cookie，使用$_COOKIE超全局变量。
//	3. 删除cookie，设置cookie时间过期即可。