<?php
/**
 * 文章删除页面
 */


//获取分类数据
//$categoryId= $_GET['category_id'];
$categoryId =$_GET["category_id"];

if (empty($categoryId)){
    echo "分类id为空";
    echo "<a href='javascript:void(0)' onclick='history.back()'></a>";
    exit();

}

//保存到数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


$existSql = "select * from article where category_id = '$categoryId' limit 1";
$existResult = $db->query($existSql);
$articleResult = $existResult->fetchAll();
if ($articleResult){
    echo "分类下有文章，请先删除、相关文章";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
 }


$sql = "delete from category where category_id = '$categoryId'";
$result = $db->exec($sql);
if($result){
    echo "删除成功,<a href='category_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "删除失败，错误信息：".$db->errorInfo()[2].",请联系管理员：2218922949@qq.com";
//    echo "<a href='javascript:history.go(-1);'>返回上一页</a>";
}