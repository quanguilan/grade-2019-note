<?php
/**
 * 文章添加页面
 */

//连接数据库
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");


//规定一个由 date_create() 返回的 DateTime 对象。该函数修改该对象。
date_default_timezone_set("PRC");


$sql= "select * from category";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);


?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="css/main.css" type="text/css"/>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<h1>博客系统</h1>
				<div id="admin-info">欢迎您，quanguilan<a href="#">退出登录</a></div>
			</div>
			<div id="left">
				<ul>
					<li><a href="category_list.php">分类管理</a></li>
					<li><a href="article_list.php">文章管理</a></li>
					<li><a>管理员</a></li>
				</ul>
			</div>
			<div id="right">
				<div id="shoye">
					<a href="#">首页</a> &gt;
					<a href="article_list.php">文章管理</a> &gt;
					<a href="#">添加文章</a>
				</div>
				<div id="table_add">
                    <form action="article_add_save.php" method="post">
						<table border="=1" cellpadding="0" cellspacing="0">
							<tr>
								<td>文章标题：</td>
								<td><input type="text" name="article_title" </td>
							</tr>
							<tr>
								<td>所属分类：</td>
								<td>
									<select name="category_id">
                                        <?php foreach ($categoryList as $row):?>
										<option value="<?php echo $row['category_id'];?> "><?php echo $row['category_name'] ?></option>
                                        <?php endforeach;?>
									</select>
								</td>
							</tr>
							<tr>
								<td>文章简介：</td>
								<td>
									<textarea name="article_intro"></textarea>
								</td>
							</tr>
                            <tr>
                                <td>文章内容：</td>
                                <td>
                                    <textarea name="article_content"></textarea>
                                </td>
                            </tr>
							<tr>
								<td></td>
								<td>
									<input type="submit" value="提交" class="btn" />
									<input type= "reset" value="重置" class="btn" />
								</td>
							</tr>
						</table>
                    </form>
                    </div>
					</div>
			</div>
		</div>
		</body>
</html>
