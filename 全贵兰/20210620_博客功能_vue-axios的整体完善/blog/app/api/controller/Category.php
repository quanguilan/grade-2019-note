<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\CategoryModel;


class Category
{
    //分类列表
    public function list()
    {
        $categoryList = CategoryModel::select();
        $data = [
            'status' =>0,
            'message' =>'',
            'data' =>[
                'categoryList' =>$categoryList,
            ]
        ];
        return json($data);
    }
}

