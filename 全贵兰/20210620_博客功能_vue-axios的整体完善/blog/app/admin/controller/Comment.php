<?php
declare (strict_types=1); // php的严格模式，弱类型

namespace app\admin\controller;

use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;


/**
 * 写功能的步骤：
 * 1. 视图先渲染出来，确定需要什么数据
 * 2. 获取参数，校验参数（可能没有）
 * 3. 使用Model处理（查询、写操作）
 * 4. 数据层model处理完成之后，把结果传递视图层view显示。
 *
 * Class Category
 * @package app\admin\controller
 */
class Comment
{
    // 评论列表
    public function index()

    {
        // 查找分类数据
        $status = Request::param('status',1);
        $commentList = CommentModel::where('status','=',$status)->select();
//        parse_str(Request::query(),$queryParams);
//        $commentList = CommentModel::paginate([
//            'list_rows' =>10,
//            'query'=>$queryParams,
//        ]);

        View::assign('commentList', $commentList);
        View::assign('status',$status);
        return View::fetch();
    }

    //回收站
    public function recycle(){
        $commentId = Request::param('comment_id');
        $validate=  Validate::rule([
            'comment_id|评论id' =>'require|between:1,'.PHP_INT_MAX,
       ]);
        if (!$validate ->check(['comment_id' =>$commentId])){
            echo $validate ->getError();
            exit();
        }
       //从数据库查找出来，赋值状态等于2；
        $comment = CommentModel::find($commentId);
        $comment['status'] =2;
        $result = $comment ->save();

        //返回json格式
        $data = [
            'status' =>$result ? 0:10001,
            'message' =>$result ? '':"修改数据库失败",
            'data' =>[
                'result' =>$result ? true:false,
            ]
        ];
        return json($data);

    }

    //恢复
    public function recover(){
        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
           'comment_id' =>'require|between:1,'.PHP_INT_MAX,
        ]);
        if (!$validate ->check(['comment_id' =>$commentId])){
             echo $validate ->getError();
            exit();
        }
        //从数据库中查找出来赋值等于1
        $comment = CommentModel::find($commentId);
        $comment['status'] = 1;
        $result = $comment ->save();

        //返回json格式
        $data = [
            'status' =>$result ? 0:10001,
            'message' =>$result ? '':"修改数据库失败",
            'data' =>[
                'result' =>$result ? true:false,
            ]
        ];
        return json($data);
    }


    //彻底删除
    public function delete(){
        $commentId = Request::param('comment_id');
        $validate = Validate::rule([
            'comment_id' =>'require|between:1,'.PHP_INT_MAX,
        ]);
        if (!$validate ->check(['comment_id' =>$commentId])){
            echo $validate ->getError();
            exit();
        }
        $result = CommentModel::destroy($commentId);
        //'url'=>"/admin/comment/index/status/2/"   使页面停留在指定页面
        //返回json格式
        $data = [
            'status' =>$result ? 0:10001,
            'message' =>$result ? '':"修改数据库失败",
            'data' =>[
                'result' =>$result ? true:false,
            ]
        ];
        return json($data);
    }

}