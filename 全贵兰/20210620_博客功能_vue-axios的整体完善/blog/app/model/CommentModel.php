<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/21
 * Time: 15:31
 */

namespace app\model;

use think\Model;

class CommentModel extends Model
{
    protected $name = 'comment'; // 表名
    protected $pk = 'comment_id'; // 主键
}
