<?php
/**
 * 创建图片
 */

//$image = imagecreate(58,25);//创建一张图片
//$white = imagecolorallocate($image,200,200,200);//设置背景颜色
//
//imagepng($image,"test.png");//以png格式输入到浏览器上或文件
//imagedestroy($image);//销毁图片


//画上随机数字
//$image = imagecreate(58,25);//创建一张图片
//$color = imagecolorallocate($image,200,200,200);//设置背景颜色
//$str = mt_rand(0,9);
//$color =imagecolorallocate($image,255,192,203);
////$font = 4;
////$x = 2;//左偏移
////$y = 5;//上偏移
////imagestring($image,$font,$x,$y,$str,$color);
//imagestring($image,4,2,5,$str,$color);
//imagepng($image,"test1.png");
//imagedestroy($image);//销毁图片



//画上四个随机数
//$image = imagecreate(50,35);
//$color = imagecolorallocate($image,156,169 ,244);
//$fontColor = imagecolorallocate($image,0,0,0);
//for ($i = 1;$i <=4;$i++){
//    $n = mt_rand(0,9);
//    imagestring($image,3,10 * $i,10,$n,$fontColor);
//
//}
//imagepng($image,"test2.png");
//imagedestroy($image);//销毁图片
//
//
//
////输入到浏览器
//header("Content-type:image/png");
//imagepng($image);//以PNG格式将图像输出到浏览器



//完整代码
$image = imagecreate(58,35);
$color = imagecolorallocate($image,255,255,255);
for($i = 1;$i<=4;$i++){
    $str = mt_rand(0,9);
    $color = imagecolorallocate($image,255,192,203);
    imagestring($image,4,2+($i-1)*15,5,$str,$color);
}
header("Content-type:image/png");
imagepng($image);
imagedestroy($image);




//总结：imageCreate()创建图片
//      imageColorAllocate()设置颜色
//      imageString()在图片上画字符串
//      imagePng()生成png图片








