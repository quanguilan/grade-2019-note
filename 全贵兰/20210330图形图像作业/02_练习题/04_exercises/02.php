<?php
//2. 生成6位随机字符。
//创建一张图片
$image = imagecreate(100,30);
//创建背景色
$color = imagecolorallocate($image,255,255 ,255);
//设置字体颜色
$color = imagecolorallocate($image,255,192,203);
$str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$max = mb_strlen($str) -1 ;
$strLen = mb_strlen($str);
for($i = 1;$i<=6;$i++){
    $n = mt_rand(0,$max);

    imagestring($image,6,10*$i,10,$str[$n],$color);
}
header("Content-type:image/png");
imagepng($image);
imagedestroy($image);