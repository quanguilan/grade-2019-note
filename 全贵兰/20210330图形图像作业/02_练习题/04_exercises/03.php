<?php
//3. 每个数字的颜色随机生成。

//创建一张图片
$image = imagecreate(100,30);
//创建背景色
$color = imagecolorallocate($image,255,255 ,255);
//设置字体颜色
$str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$max = mb_strlen($str) -1 ;
for ($i = 0;$i<6 ;$i++) {
    $red = mt_rand(0, 255);
    $green = mt_rand(0, 255);
    $blue = mt_rand(0, 255);
    $fontcolor = imagecolorallocate($image, $red, $green, $blue);
    $n = mt_rand(0,$max);
    imagestring($image,10,10*$i,10,$str[$n],$fontcolor);
}

header("Content-type:image/png");
imagepng($image);
imagedestroy($image);

